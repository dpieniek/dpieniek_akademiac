﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy.Cars
{
    /// <summary>
    /// Interaction logic for CarsPanel.xaml
    /// </summary>
    public partial class CarsPanel : Window
    {
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }        

        BazaDanych bd = new BazaDanych();

        public ObservableCollection<Car> carList { get; set; }
        public DataTable table { get; set; }
        public CarsPanel()
        {
            carList = new ObservableCollection<Car>();
            InitializeComponent();
            Car insignia = new Car();
            insignia.Name = "Opel";
            insignia.Model = "Insignia";
            insignia.Type = "kombi";
            insignia.Engine = "diesel";
            carList.Add(insignia);

            listView.ItemsSource = carList;


        }

        private void buttonWroc_Click(object sender, RoutedEventArgs e)
        {
           
            this.Hide();
            MainMenu mainMenu = new MainMenu();
            mainMenu.Show();
        }

        private void buttonDodaj_Click_1(object sender, RoutedEventArgs e)
        {
            Car auto = new Car();
            auto.Name = textBoxName.Text;
            auto.Model = textBoxModel.Text;
            auto.Type = textBoxType.Text;
            auto.Engine = textBoxEngine.Text;
            carList.Add(auto);
            textBoxName.Clear();
            textBoxModel.Clear();
            textBoxType.Clear();
            textBoxEngine.Clear();

        }

        private void buttonZamowienia_Click(object sender, RoutedEventArgs e)
        {
            if (IsWindowOpen<Window>("zamowieniaAut"))
            {
                foreach (Window window in App.Current.Windows)
                {
                    if (!window.IsActive)
                    {
                        window.Show();
                    }
                }
                this.Hide();
            }
            else
            {
                this.Hide();
                Orders.OrdersPanel ordersPanel = new Orders.OrdersPanel();
                ordersPanel.Show();
            }
        }
    }
}
    



       

    

