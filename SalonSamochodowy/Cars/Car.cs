﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonSamochodowy.Cars
{
    public class Car
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public string Engine { get; set; }
    }
}
