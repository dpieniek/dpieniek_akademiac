﻿using MySql.Data.MySqlClient;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SalonSamochodowy
{
    public class BazaDanych
    {
        public string myConnection = "datasource=db4free.net;port=3306;username=salon145ncia;password=masiej1234";
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }
        public void zamowAuto(SalonSamochodowy.Orders.OrdersPanel ordersPanel)
        {
            try
            {
                MySqlConnection myConn = new MySqlConnection(myConnection);
                string zrealizujQuery = "insert into salonsamochodowy.Orders VALUES (null, '"
                        + ordersPanel.textBoxName.Text + "', '" + ordersPanel.textBoxSurname.Text + "', '"
                        + ordersPanel.textBoxPesel.Text + "', '" + ordersPanel.textBoxPhoneNumber.Text + "', '"
                        + ordersPanel.textBoxCar.Text + "', '" + ordersPanel.textBoxCarName.Text + "', '"
                        + ordersPanel.textBoxType.Text + "', '" + ordersPanel.textBoxEngine.Text + "');";
                MySqlCommand ZapytanieZamow = new MySqlCommand(zrealizujQuery, myConn);
                MySqlDataReader zapisz;
                myConn.Open();
                zapisz = ZapytanieZamow.ExecuteReader();
                zapisz.Read();
                myConn.Close();
            }
            catch
            {
                MessageBox.Show("Brak połączenia z bazą!");
            }
        }
        public void zarejstruj(NowyUzytkownk nowyUzytkownik)
        {
            try
            {
                MySqlConnection myConn = new MySqlConnection(myConnection);
                string stworzUsera;
                stworzUsera = "insert into salonsamochodowy.Login VALUES (null, '"
                    + nowyUzytkownik.nowyUsername.Text + "', '" + nowyUzytkownik.nowyPassword.Password + "', '"
                    + nowyUzytkownik.nowyImie.Text + "', '" + nowyUzytkownik.nowyNazwisko.Text + "', '"
                    + nowyUzytkownik.nowyUlica.Text + "', '" + nowyUzytkownik.nowyNr.Text + "', '"
                    + nowyUzytkownik.nowyMiasto.Text + "', '" + nowyUzytkownik.nowyDzien.Text + "', '"
                    + nowyUzytkownik.nowyMiesiac.Text + "', '" + nowyUzytkownik.nowyRok.Text + "', '"
                    + nowyUzytkownik.nowyPesel.Text + "', '" + nowyUzytkownik.nowyTelefon.Text + "', '"
                    + nowyUzytkownik.nowyMail.Text + "');";
                MySqlCommand ZapytanieInsert = new MySqlCommand(stworzUsera, myConn);
                MySqlDataReader zapisz;
                myConn.Open();
                zapisz = ZapytanieInsert.ExecuteReader();
                zapisz.Read();
                myConn.Close();
                MessageBox.Show("Zarejestrowano użytkownika " + nowyUzytkownik.nowyUsername.Text + "!");
            }
            catch
            {
                MessageBox.Show("Brak połączenia z bazą!");
            }
        }

        public void usun(UsunUzytkownika usunUzytkownika)
        {
            try
            {
                MySqlConnection myConn = new MySqlConnection(myConnection);
                string komendaUsun;
                komendaUsun = "delete from salonsamochodowy.Login where Username='" + usunUzytkownika.textBoxUsun.Text + "'";
                MySqlCommand ZapytanieDelete = new MySqlCommand(komendaUsun, myConn);
                MySqlDataReader usunGo;
                myConn.Open();
                usunGo = ZapytanieDelete.ExecuteReader();
                usunGo.Read();
                myConn.Close();
                MessageBox.Show("Usunięto użytkownika " + usunUzytkownika.textBoxUsun.Text + "!");
            }
            catch
            {
                MessageBox.Show("Brak połączenia z serwerem!");
            }
        }
        public void zaloguj(MainWindow logowanie)
        {
            MySqlConnection myConn = new MySqlConnection(myConnection);
            string komendaSelect;
            komendaSelect = "select * from salonsamochodowy.Login where Username='"
                + logowanie.loginTextBox.Text + "' and Password='" + logowanie.passwordBox.Password + "';";
            MySqlCommand ZapytanieSelect = new MySqlCommand(komendaSelect, myConn);
            MySqlDataReader odczytaj;
            try
            {
                myConn.Open();
                odczytaj = ZapytanieSelect.ExecuteReader();
                int count = 0;
                while (odczytaj.Read())
                {
                    count = count + 1;
                }
                if (count == 1)
                {
                    var mainMenu = new MainMenu();
                    mainMenu.label.Content = "Zalogowano: " + logowanie.loginTextBox.Text;
                    mainMenu.ShowDialog();
                    logowanie.Close();
                }
                else if (count > 1)
                {
                    MessageBox.Show("Odmowa dostępu!");
                }
                else
                {
                    MessageBox.Show("Błędne dane użytkownika!");
                }
                myConn.Close();

            }
            catch (Exception)
            {
                MessageBox.Show("Brak dostępu! \nSprawdź swoje połączenie internetowe!");
            }
        }
    }
}
