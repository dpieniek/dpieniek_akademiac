﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public BazaDanych bd = new BazaDanych();
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }        
        public MainMenu()
        {
            
            InitializeComponent();
        }

        private void buttonUzytkownicy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserPanel panel = new UserPanel();
            panel.Show();
        }

        private void buttonWyloguj_Click(object sender, RoutedEventArgs e)
        {
            foreach (Window window in App.Current.Windows)
            {
                this.Close();
            }
            MainWindow logowanie = new MainWindow();
            logowanie.Show();
           
        }

        private void buttonSamochody_Click(object sender, RoutedEventArgs e)
        {
            if (IsWindowOpen<Window>("dostepneAuta"))
            {
                foreach (Window window in App.Current.Windows)
                {
                    if (!window.IsActive)
                    {
                        window.Show();
                    }
                }
                this.Close();
            }
            else
            {
                this.Close();
                Cars.CarsPanel carsPanel = new Cars.CarsPanel();
                carsPanel.Show();
            }
        }

    
    }
}
