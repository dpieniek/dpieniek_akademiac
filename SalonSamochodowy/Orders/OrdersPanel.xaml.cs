﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalonSamochodowy.Orders
{
    /// <summary>
    /// Interaction logic for OrdersPanel.xaml
    /// </summary>
    public partial class OrdersPanel : Window
    {
        BazaDanych bd = new BazaDanych();
        public OrdersPanel()
        {
            InitializeComponent();
        }
        private void buttonWroc_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            foreach (Window window in App.Current.Windows)
            {
                if (!window.IsActive)
                {
                    window.Show();
                }
            }
               
        }

        private void buttonZamow_Click(object sender, RoutedEventArgs e)
        {
            bd.zamowAuto(this);

            string daneZamowieniaTXT = "Szczegóły zamówienia: "+ DateTime.Now + " " + Environment.NewLine + "Imię zamawiającego: " + textBoxName.Text + "" + Environment.NewLine + "Nazwisko zamawiającego: " + textBoxSurname.Text + "" + Environment.NewLine + "PESEL: "
                        + textBoxPesel.Text + "" + Environment.NewLine + "Numer telefonu: " + textBoxPhoneNumber.Text + "" + Environment.NewLine + "Dane auta:" + Environment.NewLine + "Marka: "
                        + textBoxCar.Text + "" + Environment.NewLine + "Model: " + textBoxCarName.Text + "" + Environment.NewLine + "Typ: "
                        + textBoxType.Text + "" + Environment.NewLine + "Typ silnika: " + textBoxEngine.Text + "" + Environment.NewLine + "Zamówiono: ";
            string nazwaPliku = "e:\\Zamówienie_1.txt";
            System.IO.StreamWriter file = new System.IO.StreamWriter(nazwaPliku, true);
            file.WriteLine(daneZamowieniaTXT);
            file.Close();
            MessageBox.Show("Ścieżka potwierdzenia zamówienia: "+nazwaPliku);

            textBoxCar.Clear();
            textBoxCarName.Clear();
            textBoxEngine.Clear();
            textBoxPesel.Clear();
            textBoxPhoneNumber.Clear();
            textBoxSurname.Clear();
            textBoxType.Clear();
            textBoxName.Clear();

           
        }
    }
}
